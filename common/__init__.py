from .event import Event


def weighted_average(orders, depth=20):
    avg_orders = sum(map(lambda i: i[0] * i[1], orders[:depth]))
    volume_bid = sum(map(lambda i: i[1], orders[:depth]))
    avg_orders = avg_orders / volume_bid
    return avg_orders, volume_bid
