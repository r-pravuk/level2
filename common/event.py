class Event(object):
    def __init__(self):
        self.__event_handler = []

    def __iadd__(self, handler):
        self.__event_handler.append(handler)
        return self

    def __isub__(self, handler):
        self.__event_handler.remove(handler)
        return self

    def __call__(self, *args, **keywargs):
        for event_handler in self.__event_handler:
            event_handler(*args, **keywargs)
