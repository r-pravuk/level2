import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='observers',
    version='0.1.4',
    author='Roman Pravuk',
    author_email='r.pravuk@gmail.com',
    description='Level 2 Observer for Coinbase Pro',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://r-pravuk@bitbucket.org/r-pravuk/level2.git',
    project_urls={
        "repo": "https://r-pravuk@bitbucket.org/r-pravuk/level2.git"
    },
    license='MIT',
    packages=['common', 'websocket', 'observers'],
    install_requires=['python-dateutil'],
)
