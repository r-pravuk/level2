# Press the green button in the gutter to run the script.
from observers import Level2Observer

if __name__ == '__main__':
    # print_hi('PyCharm')
    observer = Level2Observer(["BTC-USD"], True)
    observer.trade["BTC-USD"] += lambda e: print(
        f"#{e['#']} {list(map(lambda a: a[0], e['asks'][:1]))} {list(map(lambda b: b[0], e['bids'][:1]))}")
    observer.run_loop()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
