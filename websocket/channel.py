class Channel:
    def __init__(self, name, product_ids):
        self.name = name.lower()
        if not product_ids:
            raise ValueError("must include at least one product id")

        if not isinstance(product_ids, list):
            product_ids = [product_ids]
        self.product_ids = set(product_ids)

    def __repr__(self):
        return str(self.as_dict())

    def as_dict(self):
        return {'name': self.name, 'product_ids': list(self.product_ids)}

    def __eq__(self, other):
        if self.name != other.name:
            raise TypeError('Channels need the same name to be compared.')
        return (self.name == other.name and
                self.product_ids == other.product_ids)

    def __add__(self, other):
        if self.name != other.name:
            raise TypeError('Channels need the same name to be added.')
        return Channel(self.name, list(self.product_ids | other.product_ids))

    def __sub__(self, other):
        if self.name != other.name:
            raise TypeError('Channels need the same name to be subtracted.')
        product_ids = self.product_ids - other.product_ids
        if not product_ids:
            return None
        return Channel(self.name, list(product_ids))
