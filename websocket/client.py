import asyncio
import base64
import hashlib
import hmac
import json
import logging
import time
from urllib.parse import urlparse

from autobahn.asyncio.websocket import WebSocketClientFactory
from autobahn.asyncio.websocket import WebSocketClientProtocol

from websocket import Channel

logger = logging.getLogger(__name__)

FEED_URL = 'wss://ws-feed.pro.coinbase.com:443'
SANDBOX_FEED_URL = 'wss://ws-feed-public.sandbox.pro.coinbase.com:443'


class ClientProtocol(WebSocketClientProtocol):

    def __call__(self):
        return self

    async def onOpen(self):
        await self.factory.on_open()

    async def onClose(self, wasClean, code, reason):
        await self.factory.on_close(wasClean, code, reason)

    async def onMessage(self, payload, isBinary):
        msg = json.loads(payload.decode('utf8'))
        if msg['type'] == 'error':
            await self.factory.on_error(msg['message'], msg.get('reason', ''))
        else:
            await self.factory.on_message(msg)


class Client(WebSocketClientFactory):
    def __init__(self, channels, feed_url=FEED_URL,
                 auth=False, key='', secret='', passphrase='',
                 auto_connect=True, auto_reconnect=True,
                 name='WebSocket Client'):
        self.coro = None

        self.connected = asyncio.Event()
        self.disconnected = asyncio.Event()
        self.disconnected.set()
        self.closing = False

        if not isinstance(channels, list):
            channels = [channels]

        self._initial_channels = channels
        self.feed_url = feed_url

        self.channels = {}
        self.subscribe(channels)

        if auth and not (key and secret and passphrase):
            raise ValueError('auth requires key, secret, and passphrase')

        self.auth = auth
        self.key = key
        self.secret = secret
        self.passphrase = passphrase

        self.auto_connect = auto_connect
        self.auto_reconnect = auto_reconnect
        self.name = name

        super().__init__(self.feed_url)

        if self.auto_connect:
            self.add_as_task_to_loop()

    def _get_subscribe_message(self, channels, unsubscribe=False, timestamp=None):
        msg_type = 'unsubscribe' if unsubscribe else 'subscribe'
        msg = {'type': msg_type,
               'channels': [channel.as_dict() for channel in channels]}

        if self.auth:
            if not timestamp:
                timestamp = str(time.time())
            message = timestamp + 'GET' + '/users/self/verify'
            message = message.encode('ascii')
            hmac_key = base64.b64decode(self.secret)
            signature = hmac.new(hmac_key, message, hashlib.sha256)
            signature_b64 = base64.b64encode(signature.digest())
            signature_b64 = signature_b64.decode('utf-8').rstrip('\n')

            msg['signature'] = signature_b64
            msg['key'] = self.key
            msg['passphrase'] = self.passphrase
            msg['timestamp'] = timestamp

        return json.dumps(msg).encode('utf8')

    def subscribe(self, channels):
        if not isinstance(channels, list):
            channels = [channels]

        sub_channels = []

        for channel in channels:
            if channel.name in self.channels:
                sub_channel = channel - self.channels[channel.name]
                if sub_channel:
                    self.channels[channel.name] += channel
                    sub_channels.append(sub_channel)

            else:
                self.channels[channel.name] = channel
                sub_channels.append(channel)

        if self.connected.is_set():
            msg = self._get_subscribe_message(sub_channels)
            self.protocol.sendMessage(msg)

    def unsubscribe(self, channels):
        if not isinstance(channels, list):
            channels = [channels]

        for channel in channels:
            if channel.name in self.channels:
                self.channels[channel.name] -= channel
                if not self.channels[channel.name]:
                    del self.channels[channel.name]

        if self.connected.is_set():
            msg = self._get_subscribe_message(channels, unsubscribe=True)
            self.protocol.sendMessage(msg)

    def add_as_task_to_loop(self):
        self.protocol = ClientProtocol()
        url = urlparse(self.url)
        self.coro = self.loop.create_connection(self, url.hostname, url.port,
                                                ssl=(url.scheme == 'wss'))
        self.loop.create_task(self.coro)

    async def on_open(self):
        self.connected.set()
        self.disconnected.clear()
        self.closing = False
        logger.info('{} connected to {}'.format(self.name, self.url))
        msg = self._get_subscribe_message(self.channels.values())
        self.protocol.sendMessage(msg)

    async def on_close(self, was_clean, code, reason):
        self.connected.clear()
        self.disconnected.set()

        msg = '{} connection to {} {}closed. {}'
        expected = 'unexpectedly ' if self.closing is False else ''

        logger.info(msg.format(self.name, self.url, expected, reason))

        if not self.closing and self.auto_reconnect:
            msg = '{} attempting to reconnect to {}.'
            logger.info(msg.format(self.name, self.url))

            self.add_as_task_to_loop()

    async def on_error(self, message, reason=''):
        logger.error('{}. {}'.format(message, reason))

    async def on_message(self, message):
        pass

    async def close(self):
        self.closing = True
        self.protocol.sendClose()
        await self.disconnected.wait()


if __name__ == '__main__':
    # A sanity check.

    logging.getLogger().setLevel(logging.DEBUG)
    logging.getLogger().addHandler(logging.StreamHandler())

    _loop = asyncio.get_event_loop()

    ws = Client([Channel('heartbeat', 'BTC-USD')])


    async def add_a_channel():
        await asyncio.sleep(1)
        ws.subscribe(Channel('heartbeat', 'LTC-USD'))
        _loop.create_task(remove_a_channel())


    async def remove_a_channel():
        await asyncio.sleep(1)
        ws.unsubscribe(Channel('heartbeat', 'BTC-USD'))


    _loop.create_task(add_a_channel())

    try:
        _loop.run_forever()
    except KeyboardInterrupt:
        _loop.run_until_complete(ws.close())
        _loop.close()
