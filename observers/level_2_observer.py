import asyncio

from dateutil.parser import isoparse

from common import Event, weighted_average
from websocket import Channel
from websocket.client import Client


class Level2Observer(Client):
    def __init__(self, product_ids, process_async=False):

        level2 = Channel("level2_50", product_ids)
        matches = Channel("matches", product_ids)
        heartbeat = Channel("heartbeat", product_ids)
        self.trade = {}
        self.bids = {}
        self.asks = {}
        self.row = {}
        self.process_async = process_async
        for product_id in product_ids:
            self.trade[product_id] = Event()
            self.bids[product_id] = {}
            self.asks[product_id] = {}
            self.row[product_id] = 0

        self.untracked_message = Event()

        super().__init__([level2, matches, heartbeat], key="a67ffa3eb7e6bfab7edabef36d192f15",
                         passphrase="mcewcfrv9j",
                         secret="2wiKhzZ7BQNWHoUtmo9kUC97hlt+PBx/e6lboQqFZkBYJKsv2SlOD9dc/LljuO9eQogOHMpa6gGXZjI7Vw6h8g==")

    async def _is_ask_incorrect(self, product_id, price):
        bids = sorted(map(lambda i: [float(i[0]), float(i[1])], list(self.bids[product_id].items())),
                      key=lambda i: i[0], reverse=True)

        return float(price) * 1.1 < bids[0][0]

    async def _is_bid_incorrect(self, product_id, price):
        asks = sorted(map(lambda i: [float(i[0]), float(i[1])], list(self.asks[product_id].items())),
                      key=lambda i: i[0])

        return float(price) > asks[0][0] * 1.1

    @staticmethod
    async def __run_event(event, *args, **keywargs):
        event(*args, **keywargs)

    async def on_message(self, message):
        message_type = message["type"]
        product_id = message["product_id"] if "product_id" in message else None

        if product_id is not None:
            if message_type == "snapshot":
                bids = list(sorted(message["bids"], key=lambda m: float(m[0]), reverse=True))[:10]
                asks = list(sorted(message["asks"], key=lambda m: float(m[0])))[:10]
                for bid in bids:
                    self.bids[product_id][bid[0]] = bid[1]
                for ask in asks:
                    self.asks[product_id][ask[0]] = ask[1]
            elif message_type == "l2update":
                changes = message["changes"]
                for change in changes:
                    side = change[0]
                    price = change[1]
                    size = change[2]
                    if float(size) != 0:
                        if side == "sell":
                            if await self._is_ask_incorrect(product_id, price):
                                self.loop.stop()
                                raise Exception("FUUUUUUUK that ask")

                            self.asks[product_id][price] = size
                        elif side == "buy":
                            if await self._is_bid_incorrect(product_id, price):
                                self.loop.stop()
                                raise Exception("FUUUUUUUK that bid")

                            self.bids[product_id][price] = size
                    else:
                        if price in self.asks[product_id]:
                            del self.asks[product_id][price]
                        if price in self.bids[product_id]:
                            del self.bids[product_id][price]
            elif message_type == "match":
                time = message["time"]
                l2state = self.__prepare_event_arg(product_id, isoparse(time))
                await self.__run_event(self.trade[product_id], l2state)

            elif message_type != "subscriptions":
                await self.__run_event(self.untracked_message, message)
        else:
            await self.__run_event(self.untracked_message, message)

    def __prepare_event_arg(self, product_id, time):
        asks = sorted(map(lambda i: [float(i[0]), float(i[1])], list(self.asks[product_id].items())),
                      key=lambda i: i[0])
        bids = sorted(map(lambda i: [float(i[0]), float(i[1])], list(self.bids[product_id].items())),
                      key=lambda i: i[0], reverse=True)

        avg_bid, volume_bid = weighted_average(bids, 10)
        avg_ask, volume_ask = weighted_average(asks, 10)

        l2state = {
            "#": self.row[product_id],
            "time": time,
            "middle_price": (avg_ask + avg_bid) / 2,
            "asks": asks,
            "ask": (avg_ask, volume_ask),
            "bids": bids,
            "bid": (avg_bid, volume_bid)
        }

        self.row[product_id] += 1
        return l2state

    def run_loop(self):
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            self.loop.run_until_complete(self.close())
        finally:
            self.loop.close()
